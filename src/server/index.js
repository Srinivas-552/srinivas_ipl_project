const fs = require('fs');
const path = require('path');

const csvToJson=require('csvtojson')

const matchesCsvFilePath = path.resolve(__dirname, '../data/matches.csv');
const deliveriesCsvFilePath = path.resolve(__dirname, '../data/deliveries.csv');


const ipl = require('./ipl');

const writeFile = (path, data) => {
    fs.writeFileSync(path, data, "utf-8", (err) => {
        if (err) {
            console.log(err);
        };
    })
}

csvToJson()
    .fromFile(matchesCsvFilePath)
    .then((matchData) => {
        let numberOfMatchesPerYear = ipl.matchesPerYear(matchData);       
        writeFile(path.resolve(__dirname, "../public/output/matchesPerYear.json"), JSON.stringify(numberOfMatchesPerYear))

        let numberOfMatchesWonPerYear = ipl.matchesWonPerTeamPerYear(matchData); 
        writeFile(path.resolve(__dirname, "../public/output/matchesWonPerTeamPerYear.json"), JSON.stringify(numberOfMatchesWonPerYear))
        

        let matchIds1 = ipl.getMatchIds(matchData,'2016');
        let matchIds2 = ipl.getMatchIds(matchData,'2015');
        // console.log(matchIds2);
        csvToJson()
            .fromFile(deliveriesCsvFilePath)
            .then((deliveriesData) => {
                let extraRunsForEachTeam = ipl.calculateExtraRunsForEachTeam(deliveriesData, matchIds1);
                writeFile(path.resolve(__dirname, "../public/output/extraRunsConcededPerTeam.json"), JSON.stringify(extraRunsForEachTeam))
                // console.log(extraRunsForEachTeam);

                let topEconomicalBowlers = ipl.getTopTenEconomicalBowlers(deliveriesData, matchIds2);
                writeFile(path.resolve(__dirname, "../public/output/topTenEconomicalBowlers.json"), JSON.stringify(topEconomicalBowlers))
                // console.log(topEconomicalBowlers)
            })
    
    });
    
