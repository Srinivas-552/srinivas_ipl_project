
function matchesPerYear(jsonObjArrData) {
    let countMatches = {}
    const output = jsonObjArrData.forEach((eachObj) => {
        let year = eachObj.season;
        if (countMatches[year]){
            countMatches[year] += 1;
        } else {
            countMatches[year] = 1;
        }
    });
    return countMatches;
};


function matchesWonPerTeamPerYear(jsonObjArrData) {
    let countWonMatches = {}
    const output = jsonObjArrData.forEach((eachObj) => {
        let year = eachObj.season;
        let winnerTeam = eachObj.winner;
        if (countWonMatches[winnerTeam]){
            if (countWonMatches[winnerTeam][year]){
                countWonMatches[winnerTeam][year] += 1;
            } else {
                if (!countWonMatches[year]){
                    countWonMatches[winnerTeam][year] = 1
                }
            }
            
        } else {
            let initialValue = [[year, 1]]
            countWonMatches[winnerTeam] = Object.fromEntries(initialValue);
        }
    });
    return countWonMatches;
    
};

function getMatchIds(matchData, year) {
    const matchIdsArray =[]
    const output = matchData.forEach( function(eachObj) {
        if (eachObj.season === year) {
            matchIdsArray.push(eachObj.id);
        }
    })
    return matchIdsArray;
};

function calculateExtraRunsForEachTeam(deliveriesData, matchIds){
    let countExtraRuns = {}
    const output = deliveriesData.forEach((eachObj) => {
        let id = eachObj.match_id;
        let team = eachObj.bowling_team;
        let extraRuns = eachObj.extra_runs;
        for (let i=0; i<matchIds.length; i++) {
            if (matchIds[i]===id){
                if (countExtraRuns[team]){
                    countExtraRuns[team] += parseFloat(extraRuns);
                } 
                else {
                    countExtraRuns[team] = parseFloat(extraRuns);
                }        
            } 
        }
    });
    return countExtraRuns;    
};


function getTopTenEconomicalBowlers(deliveriesData, matchIds){
    let bowlers = {}
    const output = deliveriesData.filter((eachObj) => {
        let id = eachObj.match_id;
        let wideRuns = eachObj.wide_runs;
        let noballRuns = eachObj.noball_runs;

        for (let i=0; i<matchIds.length; i++) {
            if (matchIds[i]===id){
                if (wideRuns==0 && noballRuns==0){

                    if(bowlers[eachObj.bowler]) {
                        bowlers[eachObj.bowler].totalBalls += 1;
                        bowlers[eachObj.bowler].totalRuns += parseFloat(eachObj.total_runs)
                        let runs = bowlers[eachObj.bowler].totalRuns;
                        let noOfBalls = bowlers[eachObj.bowler].totalBalls;
                        bowlers[eachObj.bowler].economy = ((runs*6)/noOfBalls).toFixed(2)
                    }
                    else {
                        bowlers[eachObj.bowler] = {"totalRuns": parseFloat(eachObj.total_runs), "totalBalls": 1, "economy": 0}
                    }
                }       
            } 
        }
    });
    
    let bowlersEconomyArr = Object.keys(bowlers).map(function(key){
        return bowlers[key].economy;
    });

    bowlersEconomyArr.sort(function(a, b){
        return a-b;
    });
    
    let topTenBowlers = {};
    for (let i=0; i< 10; i++){
        Object.keys(bowlers).forEach(function(eachBowler){
            if (bowlers[eachBowler].economy=== bowlersEconomyArr[i]){
                topTenBowlers[eachBowler] = bowlersEconomyArr[i];
            }
        })
    }
    return topTenBowlers;

};



module.exports = {matchesPerYear, matchesWonPerTeamPerYear, getMatchIds, calculateExtraRunsForEachTeam, getTopTenEconomicalBowlers }



